#include "StartScene.h"
#include "SplashScene.h"
#include "SimpleAudioEngine.h"
#include "PhysicsShapeCache.h"

USING_NS_CC;

Scene* SplashScene::createScene()
{
    return SplashScene::create();
}

bool SplashScene::init()
{
    if ( !Scene::init() )
    {
        return false;
    }
    //preload sounds effects
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sounds/button_normal.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sounds/button_playagain.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sounds/buttonstartgame.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sounds/crash.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sounds/bump.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sounds/lose.mp3");
    //preload background music
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("sounds/ocean_storm.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("sounds/underwater.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("sounds/bonnie.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("sounds/patakas_world.mp3");
    
    //setPhysicsShapes
    shapeCache = PhysicsShapeCache::getInstance();
    shapeCache->addShapesWithFile("shapes.plist");

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    this->scheduleOnce( schedule_selector( SplashScene::GoToStartScene), 1 );
    
    auto backgroundSprite = Sprite::create( "splash.jpg" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    
    this->addChild( backgroundSprite );

    return true;
}

void SplashScene::GoToStartScene( float dt )
{
    auto scene = StartScene::createScene();
    
    Director::getInstance( )->replaceScene( TransitionFade::create( 2, scene ) );
}
