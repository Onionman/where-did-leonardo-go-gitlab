#include "Bubble.h"
#include "Definitions.h"

USING_NS_CC;

Bubble::Bubble(Layer *layer)
{
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    
    _bubble_sprite = GameSprite::gameSpriteWithFile("bubble.png");
    _bubble_body = PhysicsBody::createCircle(_bubble_sprite->getContentSize().width/2, PhysicsMaterial(0.1f, 1.0f, 0.0f));
    
    auto width = _bubble_sprite->getContentSize().width;
    
    _bubble_sprite->setPosition(Vec2(WIDTH/2, HEIGHT - width/2));
    _bubble_sprite->setPhysicsBody(_bubble_body);
    _bubble_sprite->setTag(BUBBLE_TAG);
    _bubble_body->setDynamic(false);
    _bubble_body->setContactTestBitmask(true);
    _bubble_body->setCollisionBitmask(BUBBLE_COLL_BITMASK);
    _bubble_body->setCategoryBitmask(BUBBLE_CATHEGORY_BITMASK);
    
    layer->addChild(_bubble_sprite, 4);
}

void Bubble::Rise()
{
    //_bubble_sprite->getPhysicsBody()->applyImpulse(Vec2(0,300));
    //_bubble_body->setVelocity(Vec2(0,60));
}
