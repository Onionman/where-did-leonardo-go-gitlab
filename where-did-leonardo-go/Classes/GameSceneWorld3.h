#pragma once
#ifndef __GAMESCENEWORLD3_H__
#define __GAMESCENEWORLD3_H__

#include "cocos2d.h"
#include "Character.h"
#include "Bubble.h"
#include "Treasure.h"
#include "Turbine.h"
#include "GameSprite.h"
#include "Jellyfish.h"
#include "PhysicsShapeCache.h"

using namespace cocos2d;

class GameSceneWorld3 : public Layer
{
    Vec2 _delta;
    
    GameSprite* _ground;
    Vector<GameSprite*> _objects;
    
    bool game_over = false;
    
public:
	static Scene* createScene();

    virtual bool init();

	CREATE_FUNC(GameSceneWorld3);
    
    void TimerMethod(float dt);
    Label *_label_time;
    float _time;
    bool _time_running = false;
    
private:
    cocos2d::PhysicsWorld *sceneWorld;
    cocos2d::Point _center;
    
    PhysicsShapeCache *shapeCache;
    
    //Spielkomponenten
    Character *_leo;
    Bubble *_bubble;
    Turbine *_turbine;
    Treasure *_treasure;
    Jellyfish *_jellyfish;
    Jellyfish *_jelly2;
    GameSprite *_boost2;
    GameSprite *_boost_right;
    
    void CreateOctopussy(float width, float height);
    void CreateBoost();
    void CreateBoostRight();
    void CreateVulcan(float width, float height);
    
    //Spiellogik
    void SetPhysicsWorld(PhysicsWorld *world)
    {
        sceneWorld = world;
    };
    void StartGame();
    void ShowWinMenu();
    void ShowLoseMenu();
    void GoToNextLevel(Ref *pSender);
    void QuitGame(cocos2d::Ref *pSender);
    void update(float dt);
    
    //User Touches
    void onTouchesBegan(const std::vector<Touch*> &touches, Event* event);
    void onTouchesMoved(const std::vector<Touch*> &touches, Event* event);
    void onTouchesEnded(const std::vector<Touch*> &touches, Event* event);
    //Kollisionen
    bool onContactBegin(PhysicsContact &contact);

};
#endif //__GAMESCENEWORLD3_H__

