#include "Character.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"
#include "PhysicsShapeCache.h"


USING_NS_CC;

using namespace cocos2d;

Character::Character(Layer *layer, Vec2 pos)
{
    //PhysicsShapes
    shapeCache = PhysicsShapeCache::getInstance();
    //shapeCache->addShapesWithFile("shapes.plist");
    
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    
    _character = Sprite::create("ani/boat/boat_1.png");
    character_body = shapeCache->createBodyWithName("boat_1");
    character_body->setDynamic(false);
    character_body->setContactTestBitmask(true);
    character_body->setCategoryBitmask(CHAR_CATHEGORY_BITMASK);
    character_body->setCollisionBitmask(CHAR_COLL_BITMASK);
    character_body->setMass(BOAT_MASS);

    
    Vector<SpriteFrame*>animateFrames;
    animateFrames.reserve(5);
    animateFrames.pushBack(SpriteFrame::create("ani/boat/boat_1.png", Rect(0,0,130,90)));
    animateFrames.pushBack(SpriteFrame::create("ani/boat/boat_2.png", Rect(0,0,130,90)));
    animateFrames.pushBack(SpriteFrame::create("ani/boat/boat_3.png", Rect(0,0,130,90)));
    animateFrames.pushBack(SpriteFrame::create("ani/boat/boat_4.png", Rect(0,0,130,90)));
    animateFrames.pushBack(SpriteFrame::create("ani/boat/boat_5.png", Rect(0,0,130,90)));
    
    Animation* animation = Animation::createWithSpriteFrames(animateFrames, 0.3f);
    Animate* animate = Animate::create(animation);
    
    
    _character->runAction(RepeatForever::create(animate));
    _character->setPosition(pos);
    _character->setTag(CHAR_TAG);
    _character->setPhysicsBody(character_body);
    
    layer->addChild(_character, 10);
}

void Character::Boost()
{
    Vector<SpriteFrame*> boost_frames;
    boost_frames.reserve(12);
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_1.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_2.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_3.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_4.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_1.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_2.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_3.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_4.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_1.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_2.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_3.png", Rect(0,0,130,90)));
    boost_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_4.png", Rect(0,0,130,90)));
    
    Animation *animation_boost = Animation::createWithSpriteFrames(boost_frames, 0.3f);
    Animate *animate_boost = Animate::create(animation_boost);
    
    _character->runAction(animate_boost);
    
    _character->getPhysicsBody()->applyImpulse(Vec2(0,BOOST_POWER));
}
void Character::BoostRight()
{
    Vector<SpriteFrame*> boost_right_frames;
    boost_right_frames.reserve(1000);
    for(auto x=0; x<50; x++)
    {
        boost_right_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_1.png", Rect(0,0,130,90)));
        boost_right_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_2.png", Rect(0,0,130,90)));
        boost_right_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_3.png", Rect(0,0,130,90)));
        boost_right_frames.pushBack(SpriteFrame::create("ani/boat/boat_boost_4.png", Rect(0,0,130,90)));
    }
    
    Animation *animation_boost_right = Animation::createWithSpriteFrames(boost_right_frames, 0.03f);
    Animate *animate_boost_right = Animate::create(animation_boost_right);
    
    _character->runAction(animate_boost_right);
    
    _character->getPhysicsBody()->applyImpulse(Vec2(BOOST_POWER,0));
}

void Character::Bounce()
{
    //_character->getPhysicsBody()->applyImpulse(Vec2(40,200));
}

void Character::Blow()
{
    _character->getPhysicsBody()->applyImpulse(Vec2(WIND_POWER,0));
}

void Character::SetDynamic(bool b)
{
    character_body->setDynamic(b);
}

void Character::Die()
{
    _character->stopAllActions();
    
    Vector<SpriteFrame*> die_frames;
    die_frames.reserve(5);
    die_frames.pushBack(SpriteFrame::create("ani/boat/boat_gameover_1.png", Rect(0,0,130,90)));
    die_frames.pushBack(SpriteFrame::create("ani/boat/boat_gameover_2.png", Rect(0,0,130,90)));
    die_frames.pushBack(SpriteFrame::create("ani/boat/boat_gameover_3.png", Rect(0,0,130,90)));
    die_frames.pushBack(SpriteFrame::create("ani/boat/boat_gameover_4.png", Rect(0,0,130,90)));
    die_frames.pushBack(SpriteFrame::create("ani/boat/boat_gameover_5.png", Rect(0,0,130,90)));
    

    
    Animation *animation_die = Animation::createWithSpriteFrames(die_frames, 0.35f);
    Animate *animate_die = Animate::create(animation_die);
    
    _character->runAction(animate_die);
}

void Character::Win()
{
    _character->stopAllActions();
    
    Vector<SpriteFrame*> rep_frames;
    rep_frames.reserve(4);
    rep_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_1.png", Rect(0,0,130,90)));
    rep_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_2.png", Rect(0,0,130,90)));
    rep_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_3.png", Rect(0,0,130,90)));
    rep_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_4.png", Rect(0,0,130,90)));
    
    Animation *animation_rep = Animation::createWithSpriteFrames(rep_frames, 0.5f);
    Animate *animate_rep = Animate::create(animation_rep);
    
    _character->runAction(animate_rep);
    
    Vector<SpriteFrame*> win_frames;
    win_frames.reserve(16);
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_5.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_6.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_7.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_8.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_5.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_6.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_7.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_8.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_5.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_6.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_7.png", Rect(0,0,130,90)));
    win_frames.pushBack(SpriteFrame::create("ani/boat/boat_wins_8.png", Rect(0,0,130,90)));
    
    Animation *animation_win = Animation::createWithSpriteFrames(win_frames, 0.35f);
    Animate *animate_win = Animate::create(animation_win);
    
    _character->runAction(RepeatForever::create(animate_win));
}

void Character::Collision()
{
    Vector<SpriteFrame*> collision_frames;
    collision_frames.reserve(5);
    collision_frames.pushBack(SpriteFrame::create("ani/boat/boat_kollision_1.png", Rect(0,0,130,90)));
    collision_frames.pushBack(SpriteFrame::create("ani/boat/boat_kollision_2.png", Rect(0,0,130,90)));
    collision_frames.pushBack(SpriteFrame::create("ani/boat/boat_kollision_3.png", Rect(0,0,130,90)));
    collision_frames.pushBack(SpriteFrame::create("ani/boat/boat_kollision_4.png", Rect(0,0,130,90)));
    collision_frames.pushBack(SpriteFrame::create("ani/boat/boat_kollision_5.png", Rect(0,0,130,90)));
    
    
    Animation *animation_coll = Animation::createWithSpriteFrames(collision_frames, 0.35f);
    Animate *animate_coll = Animate::create(animation_coll);
    
    _character->runAction(animate_coll);
}

