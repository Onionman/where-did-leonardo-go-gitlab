#ifndef __TUTORIALSCENE_H__
#define __TUTORIALSCENE_H__

#include "cocos2d.h"

class TutorialScene : public cocos2d::Scene
{
    
    int _page = 1;
    Size _visibleSize;
    Vec2 _origin;
    Label *_label;
    Sprite *_page_sprite;
    
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    void nextPage();
    void prevPage();
    void menuGoBack(Ref *pSender);
    
    
    CREATE_FUNC(TutorialScene);
};

#endif // __TUTORIALSCENE_H__

