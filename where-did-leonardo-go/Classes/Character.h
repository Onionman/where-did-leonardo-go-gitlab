#ifndef __CHARACTER_H__
#define __CHARACTER_H__

#include "cocos2d.h"
#include "PhysicsShapeCache.h"

class Character
{
public:
    Character(cocos2d::Layer *layer, cocos2d::Vec2 pos);
    cocos2d::Sprite *_character;
    
    void SetDynamic(bool b);
    
    //Physics
    void Bounce();
    void Blow();
    void Boost();
    void BoostRight();
    void Die();
    void Win();
    void Collision();
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;

    PhysicsShapeCache *shapeCache;
    
    cocos2d::PhysicsBody *character_body;
};

#endif // __CHARACTER_H__

