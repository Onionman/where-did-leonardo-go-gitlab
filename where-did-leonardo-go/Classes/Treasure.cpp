#include "Treasure.h"
#include "Definitions.h"
#include "PhysicsShapeCache.h"

USING_NS_CC;

Treasure::Treasure(cocos2d::Layer *layer, Vec2 pos)
{
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
 
    //PhysicsShapes
    shapeCache = PhysicsShapeCache::getInstance();
    //shapeCache->addShapesWithFile("shapes.plist");
    
    auto treasure_sprite = Sprite::create("treasure_1.png");
    //auto treasure_body = PhysicsBody::createBox(treasure_sprite->getContentSize(), PHYSICSBODY_MATERIAL_DEFAULT);
    auto treasure_body = shapeCache->createBodyWithName("treasure_1");
    
    treasure_sprite->setPosition(pos);
    treasure_sprite->setPhysicsBody(treasure_body);
    treasure_sprite->setTag(GOAL_TAG);
    treasure_body->setDynamic(false);
    treasure_body->setContactTestBitmask(true);
    treasure_body->setCategoryBitmask(GOAL_CATHEGORY_BITMASK);
    treasure_body->setCollisionBitmask(GOAL_COLL_BITMASK);

    layer->addChild(treasure_sprite, 5);
}
