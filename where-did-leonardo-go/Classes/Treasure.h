#ifndef __TREASURE_H__
#define __TREASURE_H__

#include "cocos2d.h"
#include "PhysicsShapeCache.h"

class Treasure
{
public:
    Treasure(cocos2d::Layer *layer, cocos2d::Vec2 pos);
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
    PhysicsShapeCache *shapeCache;
    
    cocos2d::Sprite *treasure;
};

#endif // __TREASURE_H__

