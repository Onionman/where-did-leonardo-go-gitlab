#ifndef __TURBINE_H__
#define __TURBINE_H__

#include "GameSprite.h"

class Turbine
{
public:
    Turbine(cocos2d::Layer *layer);

    GameSprite *_turbine_sprite;
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
};

#endif // __TURBINE_H__

