#include "Shark.h"
#include "Definitions.h"
#include "PhysicsShapeCache.h"

USING_NS_CC;

Shark::Shark()
{
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    
    //PhysicsShapes
    shapeCache = PhysicsShapeCache::getInstance();
    //shapeCache->addShapesWithFile("shapes.plist");
}

void Shark::SpawnShark(cocos2d::Layer *layer)
{
    CCLOG("Shark::SpawnShark fired");

    auto shark_sprite = Sprite::create("ani/shark/shark_1.png");
    //auto shark_body = PhysicsBody::createBox(shark_sprite->getContentSize());
    auto shark_body = shapeCache->createBodyWithName("shark_1");
    auto shark_action = MoveBy::create(SHARK_SPEED, Vec2(-WIDTH -300, 0));
    
    Vector<SpriteFrame*>animateFrames;
    animateFrames.reserve(8);
    animateFrames.pushBack(SpriteFrame::create("ani/shark/shark_1.png", Rect(0,0,160,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/shark/shark_2.png", Rect(0,0,160,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/shark/shark_3.png", Rect(0,0,160,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/shark/shark_4.png", Rect(0,0,160,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/shark/shark_5.png", Rect(0,0,160,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/shark/shark_4.png", Rect(0,0,160,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/shark/shark_3.png", Rect(0,0,160,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/shark/shark_2.png", Rect(0,0,160,60)));
    
    
    Animation* animation = Animation::createWithSpriteFrames(animateFrames, 0.15f);
    Animate* animate = Animate::create(animation);
    
    shark_sprite->runAction(RepeatForever::create(animate));
    shark_sprite->setPosition(Vec2(WIDTH + 150, 500.0f));
    shark_sprite->setTag(ENEMY_TAG);
    shark_sprite->setPhysicsBody(shark_body);
    shark_sprite->runAction(shark_action);
    shark_body->setDynamic(false);
    shark_body->setCategoryBitmask(0x01);
    shark_body->setContactTestBitmask(true);
    shark_body->setCollisionBitmask(ENEMY_COLL_BITMASK);
    shark_body->setCategoryBitmask(ENEMY_CATHEGORY_BITMASK);
    
    layer->addChild(shark_sprite);
    
   
}
