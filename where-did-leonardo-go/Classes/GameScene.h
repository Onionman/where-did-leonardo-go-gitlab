#pragma once
#ifndef __GAMESCENE_H__
#define __GAMESCENE_H__

#include "cocos2d.h"
#include "Shark.h"
#include "Character.h"
#include "Bubble.h"
#include "Treasure.h"
#include "Turbine.h"
#include "GameSprite.h"
#include "PhysicsShapeCache.h"

using namespace cocos2d;

class GameScene : public Layer
{
    
    Vec2 _delta;
    
    GameSprite *_jellyfish;
    GameSprite* _ground;
    
    Vector<GameSprite*> _objects;
    
    bool game_over = false;
    
public:
	static Scene* createScene();

    virtual bool init();

	CREATE_FUNC(GameScene);
    
    void TimerMethod(float dt);
    
    bool _time_running = false;
    Label *_label_time;
    float _time;
    
private:
    cocos2d::PhysicsWorld *sceneWorld;
    cocos2d::Point _center;
    
    PhysicsShapeCache *shapeCache;
    
    //Spielkomponenten
    Character *_leo;
    Bubble *_bubble;
    Turbine *_turbine;
    Treasure *_treasure;
    Shark _shark;
    GameSprite *_boost;
    GameSprite *_boost_right;
    
    void SpawnShark(float dt);
    void CreateFishingNet();
    void CreateCoral(float width, float height);
    void CreateBoost();
    void CreateBoostRight();
    void CreateIce();
    
    //Spiellogik
    void SetPhysicsWorld(PhysicsWorld *world)
    {
        sceneWorld = world;
    };
    void StartGame();
    void ShowWinMenu();
    void ShowLoseMenu();
    void GoToNextLevel(Ref *pSender);
    void QuitGame(Ref *pSender);
    void update(float dt);
    
    //User Touches
    void onTouchesBegan(const std::vector<Touch*> &touches, Event* event);
    void onTouchesMoved(const std::vector<Touch*> &touches, Event* event);
    void onTouchesEnded(const std::vector<Touch*> &touches, Event* event);
    //Kollisionen
    bool onContactBegin(PhysicsContact &contact);

};
#endif //__GAMESCENE_H__

