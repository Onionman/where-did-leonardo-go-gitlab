#ifndef __SHARK_H__
#define __SHARK_H__

#include "PhysicsShapeCache.h"

class Shark
{
public:
    Shark();
    
    void SpawnShark(cocos2d::Layer *layer);
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
    PhysicsShapeCache *shapeCache;
};

#endif // __SHARK_H__

