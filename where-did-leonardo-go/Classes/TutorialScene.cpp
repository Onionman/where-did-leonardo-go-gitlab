#include "StartScene.h"
#include "TutorialScene.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* TutorialScene::createScene()
{
    return TutorialScene::create();
}

bool TutorialScene::init()
{
    if ( !Scene::init() )
    {
        return false;
    }
    
    _visibleSize = Director::getInstance()->getVisibleSize();
    _origin = Director::getInstance()->getVisibleOrigin();
    
    _label = Label::createWithTTF("1/4", "fonts/Marker Felt.ttf", 24);
    _label->setPosition(Vec2(WIDTH - 100, 50));
    this->addChild(_label, 1);
    
    _page_sprite = Sprite::create("tutorial_1.png");
    _page_sprite->setPosition(Vec2(_visibleSize.width/2 + _origin.x, _visibleSize.height/2 + _origin.y));
    
    this->addChild(_page_sprite, 0);
    
    auto menu_next = MenuItemFont::create("Weiter", CC_CALLBACK_0(TutorialScene::nextPage, this));
    auto menu_goback = MenuItemFont::create("Zurück", CC_CALLBACK_0(TutorialScene::prevPage, this));
    auto *menu = Menu::create(menu_next,
                              menu_goback,
                              NULL);
    
    menu->setPosition(0,0);
    menu_goback->setPosition(Vec2(100, 50));
    menu_next->setPosition(Vec2(200, 50));
    
    this->addChild(menu);
    
    return true;
}

void TutorialScene::nextPage()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/button_normal.mp3");
    
    switch (_page) {
        case 1:
            _page_sprite->setTexture("tutorial_2.png");
            _label->setString(std::string ("2/4"));
            break;
        case 2:
            _page_sprite->setTexture("tutorial_3.png");
            _label->setString(std::string ("3/4"));
            break;
        case 3:
            _page_sprite->setTexture("tutorial_4.png");
            _label->setString(std::string ("4/4"));
            break;
        case 4:
            this->menuGoBack(this);
            break;
        default:
            break;
    }
    _page++;
}

void TutorialScene::prevPage()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/button_normal.mp3");
    
    switch (_page) {
        case 1:
            this->menuGoBack(this);
            break;
            
        case 2:
            _page_sprite->setTexture("tutorial_1.png");
            _label->setString(std::string ("1/4"));
            break;
        case 3:
            _page_sprite->setTexture("tutorial_2.png");
            _label->setString(std::string ("2/4"));
            break;
        case 4:
            _page_sprite->setTexture("tutorial_3.png");
            _label->setString(std::string ("3/4"));
            break;
        default:
            break;
    }
    _page--;
}

void TutorialScene::menuGoBack(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/button_normal.mp3");
    auto scene = StartScene::createScene();
    Director::getInstance()->replaceScene(TransitionFlipY::create(1.0, scene));
}
