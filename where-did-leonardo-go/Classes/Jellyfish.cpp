#include "Jellyfish.h"
#include "GameSprite.h"
#include "Definitions.h"

USING_NS_CC;

Jellyfish::Jellyfish(cocos2d::Layer *layer, cocos2d::Vec2 pos)
{
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();

    _jellyfish = GameSprite::gameSpriteWithFile("ani/jelly/jelly_1.png");
    auto jelly_body = PhysicsBody::createCircle(_jellyfish->getContentSize().width/2, PhysicsMaterial(0.005f, 1.0f, 0.1f));
    
    Vector<SpriteFrame*>animateFrames;
    animateFrames.reserve(8);
    animateFrames.pushBack(SpriteFrame::create("ani/jelly/jelly_1.png", Rect(0,0,40,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/jelly/jelly_2.png", Rect(0,0,40,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/jelly/jelly_3.png", Rect(0,0,40,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/jelly/jelly_4.png", Rect(0,0,40,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/jelly/jelly_5.png", Rect(0,0,40,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/jelly/jelly_6.png", Rect(0,0,40,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/jelly/jelly_7.png", Rect(0,0,40,60)));
    animateFrames.pushBack(SpriteFrame::create("ani/jelly/jelly_8.png", Rect(0,0,40,60)));
    
    
    Animation* animation = Animation::createWithSpriteFrames(animateFrames, 0.15f);
    Animate* animate = Animate::create(animation);
    
    _jellyfish->runAction(RepeatForever::create(animate));
    _jellyfish->setPosition(pos);
    _jellyfish->setPhysicsBody(jelly_body);
    _jellyfish->setTag(JELLY_TAG);
    jelly_body->setDynamic(true);
    jelly_body->setGravityEnable(false);
    jelly_body->setContactTestBitmask(true);
    jelly_body->setCategoryBitmask(JELLY_CATHEGORY_BITMASK);
    jelly_body->setCollisionBitmask(JELLY_COLL_BITMASK);
    //jelly_body->setVelocity(Vec2(cocos2d::random(-40,40),cocos2d::random(-10,10)));
    
    layer->addChild(_jellyfish);
}

void Jellyfish::Attack()
{
    Vector<SpriteFrame*> attack_frames;
    attack_frames.reserve(3);
    attack_frames.pushBack(SpriteFrame::create("ani/jelly/jelly_coll_1.png", Rect(0,0,40,60)));
    attack_frames.pushBack(SpriteFrame::create("ani/jelly/jelly_coll_2.png", Rect(0,0,40,60)));
    attack_frames.pushBack(SpriteFrame::create("ani/jelly/jelly_coll_3.png", Rect(0,0,40,60)));
    
    Animation *animation_attack = Animation::createWithSpriteFrames(attack_frames, 0.3f);
    Animate *animate_attack = Animate::create(animation_attack);
    
    _jellyfish->runAction(animate_attack);
}
