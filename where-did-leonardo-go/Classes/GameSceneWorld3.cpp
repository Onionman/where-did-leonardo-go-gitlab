#include "Definitions.h"
#include "GameSceneWorld3.h"
#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "GameSprite.h"
#include "PhysicsShapeCache.h"
#include "StartScene.h"
#include "Jellyfish.h"
#include <iostream>

USING_NS_CC;

using namespace cocos2d;


Scene* GameSceneWorld3::createScene()
{
	auto scene = Scene::createWithPhysics();
    //scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    //scene->getPhysicsWorld()->setGravity(Vec2(5.0f, -15.0f)); Strömung nach rechts
    scene->getPhysicsWorld()->setGravity(Vec2(0, -GRAVITATION));
    
	auto layer = GameSceneWorld3::create();
    layer->SetPhysicsWorld(scene->getPhysicsWorld());
	scene->addChild(layer);
	
	return scene;
}

bool GameSceneWorld3::init()
{
	if (!Layer::init()) {
		return false;
	}
    
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/underwater.mp3");
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    _center = cocos2d::Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y);
    _delta = Vec2(0,0);
    
    shapeCache = PhysicsShapeCache::getInstance();
    
    //Spielmenü
    auto button_play = MenuItemFont::create("Start", CC_CALLBACK_0(GameSceneWorld3::StartGame, this));
    auto button_restart = MenuItemFont::create("Verlassen", CC_CALLBACK_1(GameSceneWorld3::QuitGame, this));
    auto menu = Menu::create(button_play, button_restart, NULL);
    
    menu->setPosition(0,0);
    button_play->setPosition(Point(WIDTH/2 + 100, 40));
    button_restart->setPosition(Point(WIDTH/2 - 100, 40));
    
    this->addChild(menu, 10);

    //Timer
    _label_time = Label::createWithTTF("0", "fonts/Marker Felt.ttf", 24);
    _label_time->setPosition(Point(WIDTH/2, 40));
    _time = 0;
    
    this->schedule(schedule_selector(GameSceneWorld3::TimerMethod), 0.01f);
    this->addChild(_label_time, 1);
    
    //Kollisions Rahmen um das Spielfeld
    auto edge_body = PhysicsBody::createEdgeBox(visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 3);
    auto edge_node = Node::create();
    edge_node->setPosition(_center);
    edge_node->setPhysicsBody(edge_body);
    edge_node->setTag(EDGE_TAG);
    edge_body->setContactTestBitmask(true);
    edge_body->setCategoryBitmask(ENEMY_CATHEGORY_BITMASK);
    edge_body->setCollisionBitmask(ENEMY_COLL_BITMASK);
    this->addChild(edge_node);
    
    //Ground
    auto groundBody = shapeCache->createBodyWithName("ground_world_3");
    groundBody->setDynamic(false);
    groundBody->setTag(EDGE_TAG);
    groundBody->setCategoryBitmask(ENEMY_CATHEGORY_BITMASK);
    groundBody->setCollisionBitmask(ENEMY_COLL_BITMASK);
    
    _ground = GameSprite::gameSpriteWithFile("ground_world_3.png");
    _ground->setPosition(Vec2(_center.x, 75));
    _ground->setPhysicsBody(groundBody);
    this->addChild(_ground, 1);
    
    //Spielkomponenten
    auto background_sprite = Sprite::create("background_world_3.png");
    background_sprite->setAnchorPoint(Vec2(0,0));
    background_sprite->setPosition(Vec2(0,0));
    this->addChild(background_sprite, 0);
    
    
    _leo = new Character(this, Vec2(65 + 5, visibleSize.height - 45 - 5)); //this layer!
    _bubble = new Bubble(this);
    _turbine = new Turbine(this);
    _treasure = new Treasure(this, Vec2(WIDTH - 100, 130));
    //_jellyfish = new Jellyfish(this, Vec2(WIDTH/2 + random(-200,200),HEIGHT/2 + random(-100,100)));
    //_jelly2 = new Jellyfish(this, Vec2(WIDTH/4 + random(-200,200),HEIGHT/4 + random(-100,100)));
    this->CreateOctopussy(WIDTH/2, 50);
    this->CreateBoost();
    this->CreateBoostRight();
    this->CreateVulcan(200, 0);
    _objects = Vector<GameSprite*>(4);
    _objects.pushBack(_bubble->_bubble_sprite);
    _objects.pushBack(_turbine->_turbine_sprite);
    _objects.pushBack(_boost2);
    _objects.pushBack(_boost_right);
    
    //Spiellogik
    auto listener = EventListenerTouchAllAtOnce::create();
    listener->onTouchesBegan = CC_CALLBACK_2(GameSceneWorld3::onTouchesBegan, this);
    listener->onTouchesMoved = CC_CALLBACK_2(GameSceneWorld3::onTouchesMoved, this);
    listener->onTouchesEnded = CC_CALLBACK_2(GameSceneWorld3::onTouchesEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin =  CC_CALLBACK_1(GameSceneWorld3::onContactBegin, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);
    
    this->scheduleUpdate();
    
    return true;
}
void GameSceneWorld3::StartGame()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/playagain.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/my_bonnie.mp3");
    _leo->SetDynamic(true);
    _time_running = true;
}
void GameSceneWorld3::QuitGame(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/lose.mp3");
    
    auto scene = StartScene::createScene();
    Director::getInstance()->replaceScene(scene);
}
void GameSceneWorld3::GoToNextLevel(Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/playagain.mp3");
    
    //Gibt kein nächste Level
}
void GameSceneWorld3::ShowWinMenu()
{
    auto win_label = Label::createWithTTF("G E W O N N E N !", "fonts/Marker Felt.ttf", 30);
    auto win_label2 = Label::createWithTTF("Du hast den Meeresboden erreicht! Tiefer geht es nicht!", "fonts/Marker Felt.ttf", 24);
    auto win_label3 = Label::createWithTTF("Die Entwickler Philip und Lea moechten sich zutiefst bei dir bedanken!", "fonts/Marker Felt.ttf", 24);
    auto win_label4 = Label::createWithTTF("Vielen, vielen, vielen Dank!", "fonts/Marker Felt.ttf", 24);
    win_label->setPosition(Vec2(WIDTH/2, HEIGHT - 150));
    win_label2->setPosition(Vec2(WIDTH/2, HEIGHT - 220));
    win_label3->setPosition(Vec2(WIDTH/2, HEIGHT - 290));
    win_label4->setPosition(Vec2(WIDTH/2, HEIGHT - 360));
    
    this->addChild(win_label);
    this->addChild(win_label2);
    this->addChild(win_label3);
    this->addChild(win_label4);
}
void GameSceneWorld3::ShowLoseMenu()
{
    auto lose_label = Label::createWithTTF("Du bist gesunken und ertrunken!", "fonts/Marker Felt.ttf", 30);
    auto lose_label2 = Label::createWithTTF("Klicke auf Verlassen, um es noch einmal zu versuchen.", "fonts/Marker Felt.ttf", 24);
    lose_label->setPosition(Vec2(WIDTH/2, HEIGHT - 150));
    lose_label2->setPosition(Vec2(WIDTH/2, HEIGHT - 220));
    
    this->addChild(lose_label);
    this->addChild(lose_label2);
}

void GameSceneWorld3::update(float dt)
{
    Rect tub = _turbine->_turbine_sprite->getBoundingBox();
    Rect leo = _leo->_character->getBoundingBox();
    
    if (leo.intersectsRect(tub))
    {
        CCLOG("update::intersect Turbine fired");
        _leo->Blow();
    }
    
    _turbine->_turbine_sprite->setPosition(_turbine->_turbine_sprite->getNextPosition());
    _bubble->_bubble_sprite->setPosition(_bubble->_bubble_sprite->getNextPosition());
    _boost2->setPosition(_boost2->getNextPosition());
    _boost_right->setPosition(_boost_right->getNextPosition());
}

void GameSceneWorld3::CreateVulcan(float width, float height)
{
    auto vulcan_sprite = GameSprite::gameSpriteWithFile("ani/vulkan/vulkan_1.png");
    auto vulcan_body = shapeCache->createBodyWithName("vulkan_1");
    
    Vector<SpriteFrame*>animateFrames;
    animateFrames.reserve(4);
    animateFrames.pushBack(SpriteFrame::create("ani/vulkan/vulkan_1.png", Rect(0,0,450,375)));
    animateFrames.pushBack(SpriteFrame::create("ani/vulkan/vulkan_2.png", Rect(0,0,450,375)));
    animateFrames.pushBack(SpriteFrame::create("ani/vulkan/vulkan_3.png", Rect(0,0,450,375)));
    animateFrames.pushBack(SpriteFrame::create("ani/vulkan/vulkan_4.png", Rect(0,0,450,375)));
    
    Animation* animation = Animation::createWithSpriteFrames(animateFrames, 0.5f);
    Animate* animate = Animate::create(animation);
    
    vulcan_sprite->runAction(RepeatForever::create(animate));
    vulcan_sprite->setAnchorPoint(Vec2(0,0));
    vulcan_sprite->setPosition(Vec2(width, height));
    vulcan_sprite->setPhysicsBody(vulcan_body);
    vulcan_sprite->setTag(EDGE_TAG);
    vulcan_body->setDynamic(false);
    vulcan_body->setContactTestBitmask(true);
    vulcan_body->setCategoryBitmask(ENEMY_CATHEGORY_BITMASK);
    vulcan_body->setCollisionBitmask(ENEMY_COLL_BITMASK);
    
    this->addChild(vulcan_sprite);
}

void GameSceneWorld3::CreateBoost()
{
    _boost2 = GameSprite::gameSpriteWithFile("arrow_up.png");
    
    auto width = _boost2->getContentSize().width;
    auto boost_body = PhysicsBody::createCircle(width/2, PhysicsMaterial(0.1f, 0, 1));
    
    _boost2->setPosition(Vec2(WIDTH/2 - 2*width ,HEIGHT - width/2));
    _boost2->setPhysicsBody(boost_body);
    _boost2->setTag(BOOST_TAG);
    boost_body->setDynamic(false);
    boost_body->setContactTestBitmask(true);
    boost_body->setCategoryBitmask(BOOST_CATHEGORY_BITMASK);
    boost_body->setCollisionBitmask(BOOST_COLL_BITMASK);
    
    this->addChild(_boost2, 4);
}

void GameSceneWorld3::CreateBoostRight()
{
    _boost_right = GameSprite::gameSpriteWithFile("arrow_right.png");
    
    auto width = _boost_right->getContentSize().width;
    auto boost_body = PhysicsBody::createCircle(width/2, PhysicsMaterial(0.1f, 0, 1));
    
    _boost_right->setPosition(Vec2(WIDTH/2 - 3*width ,HEIGHT - width/2));
    _boost_right->setPhysicsBody(boost_body);
    _boost_right->setTag(BOOST_RIGHT_TAG);
    boost_body->setDynamic(false);
    boost_body->setContactTestBitmask(true);
    boost_body->setCategoryBitmask(BOOST_CATHEGORY_BITMASK);
    boost_body->setCollisionBitmask(BOOST_COLL_BITMASK);
    
    this->addChild(_boost_right, 4);
}

void GameSceneWorld3::CreateOctopussy(float width, float height)
{
    auto octo_sprite = GameSprite::gameSpriteWithFile("ani/octopus/krake_1.png");
    auto octo_width = octo_sprite->getContentSize().width;
    auto octo_body = shapeCache->createBodyWithName("krake_3");
    
    Vector<SpriteFrame*>animateFrames;
    animateFrames.reserve(7);
    animateFrames.pushBack(SpriteFrame::create("ani/octopus/krake_1.png", Rect(0,0,411,411)));
    animateFrames.pushBack(SpriteFrame::create("ani/octopus/krake_2.png", Rect(0,0,411,411)));
    animateFrames.pushBack(SpriteFrame::create("ani/octopus/krake_3.png", Rect(0,0,411,411)));
    animateFrames.pushBack(SpriteFrame::create("ani/octopus/krake_4.png", Rect(0,0,411,411)));
    animateFrames.pushBack(SpriteFrame::create("ani/octopus/krake_5.png", Rect(0,0,411,411)));
    animateFrames.pushBack(SpriteFrame::create("ani/octopus/krake_6.png", Rect(0,0,411,411)));
    animateFrames.pushBack(SpriteFrame::create("ani/octopus/krake_7.png", Rect(0,0,411,411)));
    
    Animation* animation = Animation::createWithSpriteFrames(animateFrames, 0.15f);
    Animate* animate = Animate::create(animation);
    
    octo_sprite->runAction(RepeatForever::create(animate));
    
    octo_sprite->setPosition(Vec2(octo_width/2 + width,octo_width/2 + height));
    octo_sprite->setPhysicsBody(octo_body);
    octo_sprite->setTag(EDGE_TAG);
    octo_body->setDynamic(false);
    octo_body->setContactTestBitmask(true);
    octo_body->setCategoryBitmask(ENEMY_CATHEGORY_BITMASK);
    octo_body->setCollisionBitmask(ENEMY_COLL_BITMASK);

    this->addChild(octo_sprite, 3);
}

bool GameSceneWorld3::onContactBegin(cocos2d::PhysicsContact &contact)
{
    auto nodeA = contact.getShapeA()->getBody()->getNode();
    auto nodeB = contact.getShapeB()->getBody()->getNode();
    
    auto a_tag = nodeA->getTag();
    auto b_tag = nodeB->getTag();

    if (nodeA && nodeB)
    {
        //Kollision mit Wand
        if((a_tag == CHAR_TAG && b_tag == EDGE_TAG)
           ||
           (a_tag == EDGE_TAG && b_tag == CHAR_TAG))
        {
            if(!game_over)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/crash.mp3");
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/lose.mp3");
                this->ShowLoseMenu();
                game_over = true;
                _time_running = false;
                _leo->Die();
            }
        }
        //Kollision mit Bubble
        if((a_tag == CHAR_TAG && b_tag == BUBBLE_TAG)
           ||
           (a_tag == BUBBLE_TAG && b_tag == CHAR_TAG))
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/bump.mp3");
        }
        //Kollision mit Boost
        if(a_tag == CHAR_TAG && b_tag == BOOST_TAG)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/bump.mp3");
            nodeB->removeFromParentAndCleanup(true);
            _leo->Boost();
        }
        else if(a_tag == BOOST_TAG && b_tag == CHAR_TAG)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/bump.mp3");
            nodeA->removeFromParentAndCleanup(true);
            _leo->Boost();
        }
        //Kollision mit Boost Right
        if(a_tag == CHAR_TAG && b_tag == BOOST_RIGHT_TAG)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/bump.mp3");
            nodeB->removeFromParentAndCleanup(true);
            _leo->BoostRight();
        }
        else if(a_tag == BOOST_RIGHT_TAG && b_tag == CHAR_TAG)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/bump.mp3");
            nodeA->removeFromParentAndCleanup(true);
            _leo->BoostRight();
        }
        
        //Kollision mit Treasure
        if((a_tag == CHAR_TAG && b_tag == GOAL_TAG)
           ||
           (a_tag == GOAL_TAG && b_tag == CHAR_TAG))
        {
            if(!game_over)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/win.mp3");
                _time_running = false;
                game_over = true;
                _leo->Win();
                this->ShowWinMenu();
            }
        }
        //Kollision mit Jellyfish
        if((a_tag == CHAR_TAG && b_tag == JELLY_TAG)
           ||
           (a_tag == JELLY_TAG && b_tag == CHAR_TAG))
        {
            _jellyfish->Attack();
        }
    }
    

    return true;
}

void GameSceneWorld3::onTouchesBegan(const std::vector<Touch*> &touches, Event* event)
{
    for (auto touch : touches)
    {
        if (touch != nullptr)
        {
            auto tap = touch->getLocation();
            for (auto object : _objects)
            {
                if (object->boundingBox().containsPoint(tap))
                {
                    object->setTouch(touch);
                }
            }
        }
    }
}

void GameSceneWorld3::onTouchesMoved(const std::vector<Touch*> &touches, Event* event)
{
    for (auto touch : touches)
    {
        if (touch != nullptr)
        {
            auto tap = touch->getLocation();
            for (auto object : _objects)
            {
                if (object->getTouch() != nullptr && object->getTouch() == touch)
                {
                    Point nextPosition = tap;
                    object->setNextPosition(nextPosition);
                    object->setVector(Vec2(tap.x - object->getPositionX(), tap.y - object->getPositionY()));
                }
            }
        }
    }
}

void GameSceneWorld3::onTouchesEnded(const std::vector<Touch*> &touches, Event* event)
{
    for (auto touch : touches)
    {
        if (touch != nullptr)
        {
            auto tap = touch->getLocation();
            for (auto object : _objects)
            {
                if (object->getTouch() != nullptr && object->getTouch() == touch)
                {
                    //if touch ending belongs to this player, clear it
                    object->setTouch(nullptr);
                    object->setVector(Vec2(0, 0));
                }
            }
        }
    }
}

void GameSceneWorld3::TimerMethod(float dt)
{
    if(_time_running)
    {
        _time += dt;
    }
    __String *timeToDisplay = __String::createWithFormat("%.2f", _time);
    _label_time->setString(timeToDisplay->getCString());
}
