#ifndef __SPLASHSCENE_H__
#define __SPLASHSCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "PhysicsShapeCache.h"

class SplashScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    CREATE_FUNC(SplashScene);
    
private:
    PhysicsShapeCache *shapeCache;

    void GoToStartScene(float dt);
};

#endif // __SPLASHSCENE_H__
