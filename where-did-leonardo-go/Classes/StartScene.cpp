#include "StartScene.h"
#include "GameScene.h"
#include "GameSceneWorld2.h"
#include "GameSceneWorld3.h"
#include "TutorialScene.h"
#include "SimpleAudioEngine.h"
#include "PhysicsShapeCache.h"

USING_NS_CC;

Scene* StartScene::createScene()
{
    return StartScene::create();
}

bool StartScene::init()
{
    if ( !Scene::init() )
    {
        return false;
    }
    //music
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/patakas_world.mp3");
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    shapeCache = PhysicsShapeCache::getInstance();
    
    auto sprite = Sprite::create("menu_background.png");
    auto label = Label::createWithTTF("Sinking Yellow Submarine", "fonts/Marker Felt.ttf", 24);
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(label, 1);
    this->addChild(sprite, 0);
    
    auto menu_tutorial = MenuItemFont::create("Anleitung", CC_CALLBACK_1(StartScene::GoToTutorialScene, this));
    auto menu_play = MenuItemFont::create("Level 1 > 50m Tiefe", CC_CALLBACK_1(StartScene::GoToGameScene, this));
    auto menu_play2 = MenuItemFont::create("Level 2 > 1.000m Tiefe", CC_CALLBACK_1(StartScene::GoToWorld2Scene, this));
    auto menu_play3 = MenuItemFont::create("Level 3 > 10.000m Tiefe", CC_CALLBACK_1(StartScene::GoToWorld3Scene, this));

    auto menu = Menu::create(menu_tutorial, menu_play, menu_play2, menu_play3, NULL);
    
    int menu_x = visibleSize.width/2;
    int menu_y = visibleSize.height/2;
    menu->setPosition(0,0);
    menu_tutorial->setPosition(Point(menu_x, menu_y + 50.f));
    menu_play->setPosition(Point(menu_x, menu_y));
    menu_play2->setPosition(Point(menu_x, menu_y - 50));
    menu_play3->setPosition(Point(menu_x, menu_y - 100));
    

    this->addChild(menu, 1);

    return true;
}

void StartScene::GoToGameScene(cocos2d::Ref *pSender)
{
    CCLOG("StartScene::GoToGameScene fired");
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/button_playagain.mp3");
    auto scene = GameScene::createScene();
    Director::getInstance()->replaceScene(TransitionFade::create(2.0, scene));
}

void StartScene::GoToWorld2Scene(cocos2d::Ref *pSender)
{
    CCLOG("StartScene::GoToWorld2Scene fired");
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/button_playagain.mp3");
    auto scene = GameSceneWorld2::createScene();
    Director::getInstance()->replaceScene(TransitionFade::create(2.0, scene));
}

void StartScene::GoToWorld3Scene(cocos2d::Ref *pSender)
{
    CCLOG("StartScene::GoToWorld3Scene fired");
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/button_playagain.mp3");
    auto scene = GameSceneWorld3::createScene();
    Director::getInstance()->replaceScene(TransitionFade::create(2.0, scene));
}

void StartScene::GoToTutorialScene(cocos2d::Ref *pSender)
{
    CCLOG("StartScene::GoToTutorialScene fired");
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/button_normal.mp3");
    auto scene = TutorialScene::createScene();
    Director::getInstance()->replaceScene(TransitionFlipY::create(1.0, scene));
}
