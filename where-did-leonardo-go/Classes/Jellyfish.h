#ifndef __JELLYFISH_H__
#define __JELLYFISH_H__

#include "cocos2d.h"
#include "GameSprite.h"
using namespace cocos2d;

class Jellyfish
{
public:
    Jellyfish(Layer *layer, cocos2d::Vec2 pos);
    
    GameSprite *_jellyfish;
    PhysicsBody *_jelly_body;
    
    void Attack();
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
};

#endif // __JELLYFISH_H__

