#include "Turbine.h"
#include "Definitions.h"

USING_NS_CC;

Turbine::Turbine(cocos2d::Layer *layer)
{
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    
    _turbine_sprite = GameSprite::gameSpriteWithFile("ani/turbine/turbine_1.png");
    auto turbine_body = PhysicsBody::createBox(_turbine_sprite->getContentSize());
    
    Vector<SpriteFrame*>animateFrames;
    animateFrames.reserve(6);
    animateFrames.pushBack(SpriteFrame::create("ani/turbine/turbine_1.png", Rect(0,0,160,120)));
    animateFrames.pushBack(SpriteFrame::create("ani/turbine/turbine_2.png", Rect(0,0,160,120)));
    animateFrames.pushBack(SpriteFrame::create("ani/turbine/turbine_3.png", Rect(0,0,160,120)));
    animateFrames.pushBack(SpriteFrame::create("ani/turbine/turbine_4.png", Rect(0,0,160,120)));
    animateFrames.pushBack(SpriteFrame::create("ani/turbine/turbine_5.png", Rect(0,0,160,120)));
    animateFrames.pushBack(SpriteFrame::create("ani/turbine/turbine_6.png", Rect(0,0,160,120)));
    
    Animation* animation = Animation::createWithSpriteFrames(animateFrames, 0.1f);
    Animate* animate = Animate::create(animation);
    
    auto width = _turbine_sprite->getContentSize().width;
    auto height = _turbine_sprite->getContentSize().height;

    _turbine_sprite->runAction(RepeatForever::create(animate));
    _turbine_sprite->setPosition(Vec2(WIDTH/2 + width, HEIGHT - height/2));
    _turbine_sprite->setTag(TURBINE_TAG);
    _turbine_sprite->setPhysicsBody(turbine_body);
    turbine_body->setDynamic(false);
    turbine_body->setContactTestBitmask(true);
    turbine_body->setCategoryBitmask(TURBINE_CATHEGORY_BITMASK);
    turbine_body->setCollisionBitmask(TURBINE_COLL_BITMASK);
    
    layer->addChild(_turbine_sprite, 9);

}
