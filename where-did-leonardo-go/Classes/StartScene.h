#ifndef __STARTSCENE_H__
#define __STARTSCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "PhysicsShapeCache.h"

class StartScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    

    // implement the "static create()" method manually
    CREATE_FUNC(StartScene);
    
private:
    PhysicsShapeCache *shapeCache;
    
    void GoToGameScene(cocos2d::Ref *pSender);
    void GoToWorld2Scene(cocos2d::Ref *pSender);
    void GoToWorld3Scene(cocos2d::Ref *pSender);
    void GoToTutorialScene(cocos2d::Ref *pSender);
};

#endif // __STARTSCENE_H__
