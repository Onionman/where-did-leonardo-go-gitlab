#ifndef __BUBBLE_H__
#define __BUBBLE_H__

#include "cocos2d.h"
#include "GameSprite.h"
using namespace cocos2d;

class Bubble
{
public:
    Bubble(Layer *layer);
    
    GameSprite *_bubble_sprite;
    PhysicsBody *_bubble_body;
    
    void Rise();
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
};

#endif // __BUBBLE_H__

