#include "Definitions.h"
#include "GameScene.h"
#include "GameSceneWorld2.h"
#include "StartScene.h"
#include "SimpleAudioEngine.h"
#include "GameSprite.h"
#include "PhysicsShapeCache.h"
#include <iostream>

USING_NS_CC;

using namespace cocos2d;

Scene* GameScene::createScene() {
    auto scene = Scene::createWithPhysics();
    //scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    //scene->getPhysicsWorld()->setGravity(Vec2(5.0f, -15.0f)); Strömung nach rechts
    scene->getPhysicsWorld()->setGravity(Vec2(0, -GRAVITATION));
    
    auto layer = GameScene::create();
    layer->SetPhysicsWorld(scene->getPhysicsWorld());
    scene->addChild(layer);
    
    return scene;
}

bool GameScene::init() {
    if (!Layer::init()) {
        return false;
    }
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/underwater.mp3");
    shapeCache = PhysicsShapeCache::getInstance();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    _center = cocos2d::Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y);
    _delta = Vec2(0,0);
    
    //Spielmenü
    auto button_play = MenuItemFont::create("Start", CC_CALLBACK_0(GameScene::StartGame, this));
    auto button_restart = MenuItemFont::create("Verlassen", CC_CALLBACK_1(GameScene::QuitGame, this));
    auto menu = Menu::create(button_play, button_restart, NULL);
    
    menu->setPosition(0,0);
    button_play->setPosition(Point(WIDTH/2 + 100, 40));
    button_restart->setPosition(Point(WIDTH/2 - 100, 40));
    
    this->addChild(menu, 10);
    
    //Timer
    _label_time = Label::createWithTTF("0", "fonts/Marker Felt.ttf", 24);
    _label_time->setPosition(Point(WIDTH/2, 40));
    
    this->schedule(schedule_selector(GameScene::TimerMethod), 0.01f);
    this->addChild(_label_time, 1);

    
    //Kollisions Rahmen um das Spielfeld
    auto edge_body = PhysicsBody::createEdgeBox(visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 3);
    auto edge_node = Node::create();
    edge_node->setPosition(_center);
    edge_node->setPhysicsBody(edge_body);
    edge_node->setTag(EDGE_TAG);
    edge_body->setContactTestBitmask(true);
    edge_body->setCategoryBitmask(ENEMY_CATHEGORY_BITMASK);
    edge_body->setCollisionBitmask(ENEMY_COLL_BITMASK);
    this->addChild(edge_node);
    
    //Ground
    //auto ground_body = PhysicsBody::createBox(Size(WIDTH, 64),PhysicsMaterial(0.1f, 1.0f, 0.5f));
    auto ground_body = shapeCache->createBodyWithName("ground_world_1");
    ground_body->setDynamic(false);
    ground_body->setContactTestBitmask(true);
    ground_body->setCategoryBitmask(ENEMY_CATHEGORY_BITMASK);
    ground_body->setCollisionBitmask(ENEMY_COLL_BITMASK);
    
    _ground = GameSprite::gameSpriteWithFile("ground_world_1.png");
    _ground->setPosition(Vec2(_center.x, 32));
    _ground->setPhysicsBody(ground_body);
    _ground->setTag(EDGE_TAG);
    this->addChild(_ground);
    
    //Spielkomponenten
    auto background_sprite = Sprite::create("background_world_1.png");
    background_sprite->setAnchorPoint(Vec2(0,0));
    background_sprite->setPosition(Vec2(0,0));
    this->addChild(background_sprite);
    
    this->schedule(schedule_selector(GameScene::SpawnShark), SPAWN_SHARK_FREQ);
    
    _leo = new Character(this, Vec2(90 + 65, visibleSize.height - 45)); //this layer!
    _bubble = new Bubble(this);
    _turbine = new Turbine(this);
    _treasure = new Treasure(this, Vec2(WIDTH - 220,80));
    this->CreateBoost();
    this->CreateBoostRight();
    this->CreateFishingNet();
    this->CreateIce();
    this->CreateCoral(250, 20);
    _objects = Vector<GameSprite*>(4);
    _objects.pushBack(_bubble->_bubble_sprite);
    _objects.pushBack(_turbine->_turbine_sprite);
    _objects.pushBack(_boost);
    _objects.pushBack(_boost_right);
    
    //Spiellogik
    auto listener = EventListenerTouchAllAtOnce::create();
    listener->onTouchesBegan = CC_CALLBACK_2(GameScene::onTouchesBegan, this);
    listener->onTouchesMoved = CC_CALLBACK_2(GameScene::onTouchesMoved, this);
    listener->onTouchesEnded = CC_CALLBACK_2(GameScene::onTouchesEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin =  CC_CALLBACK_1(GameScene::onContactBegin, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);
    
    this->scheduleUpdate();
    
    return true;
}
void GameScene::StartGame()
{
    CCLOG("GameScene::StartGame");
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/playagain.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/my_bonnie.mp3");
    _leo->SetDynamic(true);
    _time_running = true;
}
void GameScene::QuitGame(cocos2d::Ref *pSender)
{
    CCLOG("GameScene::QuitGame");
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/lose.mp3");
    
    auto scene = StartScene::createScene();
    Director::getInstance()->replaceScene(scene);
}
void GameScene::GoToNextLevel(Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/playagain.mp3");
    
    auto scene = GameSceneWorld2::createScene();
    Director::getInstance()->replaceScene(scene);
}
void GameScene::ShowWinMenu()
{
    auto win_label = Label::createWithTTF("Du hast es geschafft!", "fonts/Marker Felt.ttf", 30);
    auto win_label2 = Label::createWithTTF("Aber kannst du noch tiefer sinken?", "fonts/Marker Felt.ttf", 24);
    win_label->setPosition(Vec2(WIDTH/2, HEIGHT - 150));
    win_label2->setPosition(Vec2(WIDTH/2, HEIGHT - 220));
    auto button_quit = MenuItemFont::create("Nächstes Level", CC_CALLBACK_1(GameScene::GoToNextLevel, this));
    auto win_menu = Menu::create(button_quit, NULL);
    
    win_menu->setPosition(0,0);
    button_quit->setPosition(Point(WIDTH/2, HEIGHT/2 - 70));
    
    this->addChild(win_label);
    this->addChild(win_label2);
    this->addChild(win_menu, 10);
}
void GameScene::ShowLoseMenu()
{
    auto lose_label = Label::createWithTTF("Du bist gesunken und ertrunken!", "fonts/Marker Felt.ttf", 30);
    auto lose_label2 = Label::createWithTTF("Klicke auf Verlassen, um es noch einmal zu versuchen.", "fonts/Marker Felt.ttf", 24);
    lose_label->setPosition(Vec2(WIDTH/2, HEIGHT - 150));
    lose_label2->setPosition(Vec2(WIDTH/2, HEIGHT - 220));
    
    this->addChild(lose_label);
    this->addChild(lose_label2);
}
void GameScene::update(float dt)
{
    Rect tub = _turbine->_turbine_sprite->getBoundingBox();
    Rect leo = _leo->_character->getBoundingBox();
    
    if (leo.intersectsRect(tub))
    {
        CCLOG("update::intersect Turbine fired");
        _leo->Blow();
    }
    
    _turbine->_turbine_sprite->setPosition(_turbine->_turbine_sprite->getNextPosition());
    _bubble->_bubble_sprite->setPosition(_bubble->_bubble_sprite->getNextPosition());
    _boost->setPosition(_boost->getNextPosition());
    _boost_right->setPosition(_boost_right->getNextPosition());
}
void GameScene::SpawnShark(float dt)
{
    _shark.SpawnShark( this );
}

void GameScene::CreateCoral(float width, float height)
{
    auto coral_sprite = GameSprite::gameSpriteWithFile("coral.png");
    
    auto coral_width = coral_sprite->getContentSize().width;
    
    //auto coral_body = PhysicsBody::createCircle(width/2, PhysicsMaterial(0.1f, 0, 1));
    auto coral_body = shapeCache->createBodyWithName("coral");
    
    coral_sprite->setPosition(Vec2(coral_width/2 + width,coral_width/2 + height));
    coral_sprite->setPhysicsBody(coral_body);
    coral_sprite->setTag(ENEMY_TAG);
    coral_body->setDynamic(false);
    coral_body->setContactTestBitmask(true);
    coral_body->setCategoryBitmask(ENEMY_CATHEGORY_BITMASK);
    coral_body->setCollisionBitmask(ENEMY_COLL_BITMASK);
    
    this->addChild(coral_sprite);
}

void GameScene::CreateFishingNet()
{
    auto net_sprite = GameSprite::gameSpriteWithFile("fishing_net_2.png");
    
    //auto net_body = PhysicsBody::createCircle(width/2, PhysicsMaterial(0.1f, 0, 1));
    auto net_body = shapeCache->createBodyWithName("fishing_net_2");
    
    net_sprite->setPosition(Vec2(WIDTH/2 + 100 ,HEIGHT/2 + 100));
    net_sprite->setPhysicsBody(net_body);
    net_sprite->setTag(ENEMY_TAG);
    net_body->setDynamic(false);
    net_body->setContactTestBitmask(true);
    net_body->setCategoryBitmask(ENEMY_CATHEGORY_BITMASK);
    net_body->setCollisionBitmask(ENEMY_COLL_BITMASK);
    
    this->addChild(net_sprite);
}

void GameScene::CreateIce()
{
    auto ice_sprite = GameSprite::gameSpriteWithFile("ice_world_1.png");
    
    auto width = ice_sprite->getContentSize().width;
    auto ice_body = shapeCache->createBodyWithName("ice_world_1");
    
    ice_sprite->setPosition(Vec2(50 ,HEIGHT - width/2 -50));
    ice_sprite->setPhysicsBody(ice_body);
    ice_sprite->setTag(ENEMY_TAG);
    ice_body->setDynamic(false);
    ice_body->setContactTestBitmask(true);
    ice_body->setCategoryBitmask(ENEMY_CATHEGORY_BITMASK);
    ice_body->setCollisionBitmask(ENEMY_COLL_BITMASK);
    
    this->addChild(ice_sprite);
}


void GameScene::CreateBoost()
{
    _boost = GameSprite::gameSpriteWithFile("arrow_up.png");
    
    auto width = _boost->getContentSize().width;
    
    auto boost_body = PhysicsBody::createCircle(width/2, PhysicsMaterial(0.1f, 0, 1));
    
    _boost->setPosition(Vec2(WIDTH/2 - 2*width ,HEIGHT - width/2));
    _boost->setPhysicsBody(boost_body);
    _boost->setTag(BOOST_TAG);
    boost_body->setDynamic(false);
    boost_body->setContactTestBitmask(true);
    boost_body->setCategoryBitmask(BOOST_CATHEGORY_BITMASK);
    boost_body->setCollisionBitmask(BOOST_COLL_BITMASK);
    
    this->addChild(_boost);
}

void GameScene::CreateBoostRight()
{
    _boost_right = GameSprite::gameSpriteWithFile("arrow_right.png");
    
    auto width = _boost_right->getContentSize().width;
    
    auto boost_body = PhysicsBody::createCircle(width/2, PhysicsMaterial(0.1f, 0, 1));
    
    _boost_right->setPosition(Vec2(WIDTH/2 - 3*width ,HEIGHT - width/2));
    _boost_right->setPhysicsBody(boost_body);
    _boost_right->setTag(BOOST_RIGHT_TAG);
    boost_body->setDynamic(false);
    boost_body->setContactTestBitmask(true);
    boost_body->setCategoryBitmask(BOOST_CATHEGORY_BITMASK);
    boost_body->setCollisionBitmask(BOOST_COLL_BITMASK);
    
    this->addChild(_boost_right, 4);
}

bool GameScene::onContactBegin(cocos2d::PhysicsContact &contact)
{
    //PhysicsBody *a = contact.getShapeA()->getBody();
    //PhysicsBody *b = contact.getShapeB()->getBody();
    
    auto nodeA = contact.getShapeA()->getBody()->getNode();
    auto nodeB = contact.getShapeB()->getBody()->getNode();
    
    auto a_tag = nodeA->getTag();
    auto b_tag = nodeB->getTag();
    
    //auto contactpointx = contact.getContactData()->normal.x;
    //auto contactpointy = contact.getContactData()->normal.y;
    
    //int collisionSide;
    
    
    if (nodeA && nodeB)
    {
        //Kollision mit Wand
        if((a_tag == CHAR_TAG && b_tag == EDGE_TAG)
           ||
           (a_tag == EDGE_TAG && b_tag == CHAR_TAG))
        {
            
            if(!game_over)
            {
                    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/crash.mp3");
                    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/lose.mp3");
                    this->ShowLoseMenu();
                    _time_running = false;
                    game_over = true;
                    _leo->Die();
            }
            //auto scene = GameOverScene::createScene();
            //Director::getInstance()->pushScene(TransitionMoveInB::create(2, scene));
        }
        //Kollision mit Bubble
        if((a_tag == CHAR_TAG && b_tag == BUBBLE_TAG)
           ||
           (a_tag == BUBBLE_TAG && b_tag == CHAR_TAG))
        {
            CCLOG("Collision Bubble");
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/bump.mp3");
            
            _bubble->Rise();
            _leo->Bounce();
        }
        //Kollision mit Boost
        if(a_tag == CHAR_TAG && b_tag == BOOST_TAG)
        {
            CCLOG("Collision Boost");
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/bump.mp3");
            nodeB->removeFromParentAndCleanup(true);
            _leo->Boost();
        }
        else if(a_tag == BOOST_TAG && b_tag == CHAR_TAG)
        {
            CCLOG("Collision Boost");
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/bump.mp3");
            nodeA->removeFromParentAndCleanup(true);
            _leo->BoostRight();
        }
        //Kollision mit Boost
        if(a_tag == CHAR_TAG && b_tag == BOOST_RIGHT_TAG)
        {
            CCLOG("Collision Boost");
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/bump.mp3");
            nodeB->removeFromParentAndCleanup(true);
            _leo->Boost();
        }
        else if(a_tag == BOOST_RIGHT_TAG && b_tag == CHAR_TAG)
        {
            CCLOG("Collision Boost");
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/bump.mp3");
            nodeA->removeFromParentAndCleanup(true);
            _leo->BoostRight();
        }
        //Kollision mit Treasure
        if((a_tag == CHAR_TAG && b_tag == GOAL_TAG)
           ||
           (a_tag == GOAL_TAG && b_tag == CHAR_TAG))
        {
            CCLOG("Collision Treasure");
            
            if(!game_over)
            {
                    CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
                    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/win.mp3");
                    _time_running = false;
                    _leo->Win();
                    game_over = true;
                    this->ShowWinMenu();
            }

            //auto scene = WinScene::createScene();
            //Director::getInstance()->pushScene(TransitionMoveInT::create(1, scene));
        }
        //Kollision mit Turbine
        if((a_tag == CHAR_TAG && b_tag == TURBINE_TAG)
           ||
           (a_tag == TURBINE_TAG && b_tag == CHAR_TAG))
        {
            CCLOG("Collision Turbine");
            _leo->Blow();
        }
        //Kollision mit Enemy
        if((a_tag == CHAR_TAG && b_tag == ENEMY_TAG)
           ||
           (a_tag == ENEMY_TAG && b_tag == CHAR_TAG))
        {
            CCLOG("Collision Enemy");
            _leo->Collision();
        }
    }
    
    return true;
}

void GameScene::onTouchesBegan(const std::vector<Touch*> &touches, Event* event)
{
    for (auto touch : touches) {
        if (touch != nullptr) {
            auto tap = touch->getLocation();
            for (auto object : _objects) {
                if (object->boundingBox().containsPoint(tap)) {
                    object->setTouch(touch);
                }
            }
        }
    }
}

void GameScene::onTouchesMoved(const std::vector<Touch*> &touches, Event* event)
{
    for (auto touch : touches) {
        if (touch != nullptr) {
            auto tap = touch->getLocation();
            for (auto object : _objects) {
                if (object->getTouch() != nullptr && object->getTouch() == touch) {
                    Point nextPosition = tap;
                    object->setNextPosition(nextPosition);
                    object->setVector(Vec2(tap.x - object->getPositionX(), tap.y - object->getPositionY()));
                }
            }
        }
    }
}

void GameScene::onTouchesEnded(const std::vector<Touch*> &touches, Event* event)
{
    for (auto touch : touches) {
        if (touch != nullptr) {
            auto tap = touch->getLocation();
            for (auto object : _objects) {
                if (object->getTouch() != nullptr && object->getTouch() == touch) {
                    //if touch ending belongs to this player, clear it
                    object->setTouch(nullptr);
                    object->setVector(Vec2(0, 0));
                }
            }
        }
    }
}


 void GameScene::TimerMethod(float dt)
 {
     if(_time_running)
     {
         _time += dt;
     }
     
     __String *timeToDisplay = __String::createWithFormat("%.2f", _time);
     _label_time->setString(timeToDisplay->getCString());
 }

