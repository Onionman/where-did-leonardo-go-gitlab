#define GOAL_WIDTH 400
#define SCALE_RATIO 200
#define COCOS2D_DEBUG 1
#include "GameLayer.h"

#include <iostream>

using namespace cocos2d;
Label *label_score;
Scene* _scene;
bool new_ball = false;
bool new_bucket = false;

char* set_score_label(int _score) {
    char text[256];
    sprintf(text, "Score: %d", _score);
    
    return text;
}

void GameLayer::ballcreate() {
    
    auto ballBody = PhysicsBody::createCircle(
                                              17.5f,
                                              PhysicsMaterial(1.0f, 0.3f, 1.0f)
                                              );
    ballBody->setDynamic(true);
    ballBody->setContactTestBitmask(0xFFFFFFFF);
    ballBody->setCategoryBitmask(0x02);    // 0011
    ballBody->setCollisionBitmask(0x01);   // 0001
    ballBody->setMass(10.0f);
    _ball = GameSprite::gameSpriteWithFile("res/ball.png");
    _ball->setPosition(Vec2(400.0f, 500.0f));
    _ball->setTag(100);
    _ball->setPhysicsBody(ballBody);
    
    this->addChild(_ball);
}

void GameLayer::bucketcreate() {
    //Small bucket
    _bucket = GameSprite::gameSpriteWithFile("res/eimer.png");
    auto bucketBody = PhysicsBody::createBox(
                                             Size(_bucket->getContentSize().width, _bucket->getContentSize().height),
                                             PhysicsMaterial(0.0f, 0.2f, 0.5f)
                                             );
    bucketBody->setDynamic(true);
    bucketBody->setContactTestBitmask(0xFFFFFFFF);
    bucketBody->setCategoryBitmask(0x01);    // 0001
    bucketBody->setCollisionBitmask(0x02);   // 0011
    //bucketBody->setMass(5.0f);
    _bucket->setPosition(Vec2(_center.x, 102.0f));
    _bucket->setPhysicsBody(bucketBody);
    _bucket->setTag(200);
    
    this->addChild(_bucket, 2);
}

GameLayer::GameLayer() {}

GameLayer::~GameLayer(){}

Scene* GameLayer::scene() {
	auto scene = Scene::createWithPhysics();;
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    scene->getPhysicsWorld()->setGravity(Vec2(0.0f, -350.0f));
    _scene = scene;
    
	auto layer = GameLayer::create();

	scene->addChild(layer);
	

	return scene;
}

bool GameLayer::init() {
	// call to super
	if (!Layer::init()) {
		return false;
	}
    

	Vec2 origin = Director::getInstance()->getVisibleOrigin();
    auto visibleSize = Director::getInstance()->getVisibleSize();
	_screenSize = Director::getInstance()->getWinSize();
	_center = Vec2(_screenSize.width * 0.5, _screenSize.height * 0.5);
	_delta = Vec2(0,0);
	
    label_score = Label::createWithTTF(set_score_label(score), "fonts/Marker Felt.ttf", 24);
    label_score->setPosition(Vec2(origin.x +  label_score->getContentSize().width, origin.y +visibleSize.height - label_score->getContentSize().height));
    this->addChild(label_score, 1);

    //Walls
   
    auto wallBody1 = PhysicsBody::createBox(
                                             Size(32.0f, origin.y + visibleSize.height),
                                             PhysicsMaterial(0.1f, 1.0f, 0.5f)
                                             );
    auto wallBody2 = PhysicsBody::createBox(
                                           Size(32.0f,  origin.y + visibleSize.height),
                                           PhysicsMaterial(0.1f, 1.0f, 0.5f)
                                           );

    
    wallBody1->setDynamic(false);
    wallBody1->setPositionOffset(Vec2(visibleSize.width, visibleSize.height-visibleSize.height/2));
    wallBody1->setName("wall1");
    wallBody1->setTag(1);
    
    wallBody2->setDynamic(false);
    wallBody2->setPositionOffset(Vec2(-visibleSize.width/2, 0));
    wallBody2->setName("wall2");
    wallBody2->setTag(2);
    
    
    this->addComponent(wallBody1);
    this->addComponent(wallBody2);
    
    
    //Ground
	auto groundBody = PhysicsBody::createBox(
		Size(_screenSize.width, 32.0f),
		PhysicsMaterial(0.1f, 1.0f, 0.5f)
	);

	groundBody->setDynamic(false);

	_ground = GameSprite::gameSpriteWithFile("res/ground.png");
	_ground->setPosition(Vec2(_center.x, 16.0f));
	_ground->setPhysicsBody(groundBody);
    this->addChild(_ground);

    bucketcreate();
    ballcreate();

    
	auto listener = EventListenerTouchAllAtOnce::create();
	listener->onTouchesBegan = CC_CALLBACK_2(GameLayer::onTouchesBegan, this);
	listener->onTouchesMoved = CC_CALLBACK_2(GameLayer::onTouchesMoved, this);
	listener->onTouchesEnded = CC_CALLBACK_2(GameLayer::onTouchesEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(GameLayer::onContactBegin, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

	//create main loop
	this->scheduleUpdate();
    
	return true;
}


void GameLayer::check() {

    if (_ball->getPosition().x > 800 or _ball->getPosition().x < 0) {
        new_ball = true;
        _ball->removeFromParentAndCleanup(true);
    }
    
    if (_bucket->getRotation() > 90 or _bucket->getRotation() < -90) {
        new_bucket = true;
        _bucket->removeFromParentAndCleanup(true);
        new_ball = true;
        _ball->removeFromParentAndCleanup(true);
        
    }
}



void GameLayer::update(float dt) {
    
    label_score->setString(set_score_label(score));

    if (new_ball) {
        ballcreate();
        new_ball = false;
    }
    
    if (new_bucket) {
        bucketcreate();
        new_bucket = false;
    }
    
    check();
    
}

bool GameLayer::onContactBegin(PhysicsContact& contact) {
    auto nodeA = contact.getShapeA()->getBody()->getNode();
    auto nodeB = contact.getShapeB()->getBody()->getNode();
    
    auto contactpointx = contact.getContactData()->normal.x;
    auto contactpointy = contact.getContactData()->normal.y;
   
    int collisionSide;
    
    if (nodeA && nodeB){
        if (fabs(round(contactpointx)) == 0 && fabs(round(contactpointy)) == 1) {
            collisionSide = 2; // top
            log("top");
        } else if (round(contactpointx) == 1 && round(contactpointy) == -0) {
            collisionSide = 1; // left
            log("left");
        } else if (round(contactpointx) == -1 && round(contactpointy) == 0) {
            collisionSide = 3; // right
            log("right");
        }
        
        if(collisionSide == 1) {
            //
        } else if(collisionSide == 2) {
            if(nodeA->getTag() == 100) {
                nodeA->removeFromParentAndCleanup(true);
            } else if (nodeB->getTag() == 100) {
                nodeB->removeFromParentAndCleanup(true);
            }
            new_ball = true;
            score += 1;
        } else if(collisionSide == 3) {
            //
        }
    }
    
    return true;
}

void GameLayer::onTouchesBegan(const std::vector<Touch*> &touches, Event* event) {
	for (auto touch : touches) {
		if (touch != nullptr) {
			_delta = touch->getLocation();
		}
	}
}

void GameLayer::onTouchesMoved(const std::vector<Touch*> &touches, Event* event){
	for (auto touch : touches) {
		if (touch != nullptr) {
			_tap = touch->getLocation();
		}
	}
}

void GameLayer::onTouchesEnded(const std::vector<Touch*> &touches, Event* event) {
	for (auto touch : touches) {
		if (touch != nullptr) {
			Vec2 tap = touch->getLocation();
			_force = Vec2( (tap.x - _delta.x)*10.0f, 350 *10.0f + (tap.y - _delta.y));
			CCLOG("Force: %f %f", _force.x, _force.y);
			_ball->getPhysicsBody()->applyImpulse(_force);
		}
	}
}






