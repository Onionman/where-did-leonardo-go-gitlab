#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init() override;
	void update(float) override;

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

private:
	cocos2d::Sprite* player;
	int player_speed; 
	int enemy_timer;
	int spawn_timer;
	int enemy_speed; 
	int score;
	std::vector<cocos2d::Sprite*> projectiles;
	std::vector<cocos2d::Sprite*> enemies;
	bool shooting;
	int shooting_timer;
	cocos2d::Label* score_label;
};

#endif // __HELLOWORLD_SCENE_H__
