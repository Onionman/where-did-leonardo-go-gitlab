#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
 
    if ( !Layer::init() )
    {
        return false;
    }

	player_speed = 0;
	enemy_speed = 120; 
	enemy_timer = 0; 
	spawn_timer = 0; 
	score = 0; 
	shooting_timer = 20;
	shooting = false;

	player = Sprite::create("tank.png");
	player->setScale(0.2);
	player->setPosition(this->getContentSize().width/2, player->getBoundingBox().size.height);

	std::string text = std::to_string(score);
	score_label = Label::createWithSystemFont(text, "Arial", 60);
	score_label->setPosition(50, 50);
	this->addChild(score_label, 5); 

    this->addChild(player, 0);

	float enemy_x = 80;
	float enemy_y = this->getContentSize().height;

	for (int i = 0; i < 7; i++) {
		auto enemy = Sprite::create("alien.png");
		enemy->setPosition(enemy_x, enemy_y);
		this->addChild(enemy);
		enemies.push_back(enemy);
		enemy_x = enemy_x + enemy->getBoundingBox().size.width + 10; 
	}

    auto eventListener = EventListenerKeyboard::create();

    eventListener->onKeyPressed = [&](EventKeyboard::KeyCode keyCode, Event* event){

        Vec2 loc = event->getCurrentTarget()->getPosition();
        switch(keyCode){
            case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
            case EventKeyboard::KeyCode::KEY_A:
                player_speed = -10;
                break;
            case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
            case EventKeyboard::KeyCode::KEY_D:
                player_speed = 10; 
                break;
			case EventKeyboard::KeyCode::KEY_SPACE:
                shooting = true; 
                break;
        }
    };

	eventListener->onKeyReleased = [&](EventKeyboard::KeyCode keyCode, Event* event){

        Vec2 loc = event->getCurrentTarget()->getPosition();
        switch(keyCode){
            case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
            case EventKeyboard::KeyCode::KEY_A:
                player_speed = 0;
                break;
            case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
            case EventKeyboard::KeyCode::KEY_D:
                player_speed = 0;
                break;
			case EventKeyboard::KeyCode::KEY_SPACE:
				shooting = false;
				
                break;
        }
    };

    this->_eventDispatcher->addEventListenerWithSceneGraphPriority(eventListener, this);
	this->scheduleUpdate(); 
    return true;
}

void HelloWorld::update(float) {
	Vec2 pos = player->getPosition();
	if ((player_speed > 0 && pos.x + player->getBoundingBox().size.width < this->getContentSize().width) || (player_speed <  player->getBoundingBox().size.width/2 && pos.x > 0)) {
		player->setPosition(pos.x + player_speed, pos.y);
	}

	int enemy_hit = -1; 
	int projectile_hit = -1; 

	for (int p = 0; p < projectiles.size(); p++) {
		for (int e = 0; e < enemies.size(); e++) {
			auto rect1 = enemies[e]->getBoundingBox(); 
			auto rect2 = projectiles[p]->getBoundingBox(); 
			if(rect1.intersectsRect(rect2)) {
				enemy_hit = e; 
				projectile_hit = p; 
			}
		}
	}

	if (enemy_hit != -1) {
		this->removeChild(enemies[enemy_hit]);
		this->removeChild(projectiles[projectile_hit]);
		enemies.erase(enemies.begin() + enemy_hit); 
		projectiles.erase(projectiles.begin() + projectile_hit);
		score++;
		std::string text = std::to_string(score);
		score_label->setString(text);
	}

	auto move = MoveBy::create(0.5, Vec2(0, -10));

	if (enemy_timer == 0) {
		for (int i = 0; i < enemies.size(); i++)
		{
			enemies[i]->runAction(move->clone());
		}
		enemy_timer = enemy_speed;
		spawn_timer++;
	}


	if (spawn_timer == 15 || enemies.empty()) {
		float enemy_x = 150;
		float enemy_y = this->getContentSize().height;

		if (enemies.empty() && enemy_speed > 30) 
		{
			enemy_speed = enemy_speed - 30;
		}
		else if (enemy_speed > 20) {
			enemy_speed = enemy_speed - 20;
		}

		for (int i = 0; i < 7; i++) {
		auto enemy = Sprite::create("alien.png");
		enemy->setPosition(enemy_x, enemy_y);
		this->addChild(enemy);
		enemies.push_back(enemy);
		enemy_x = enemy_x + enemy->getBoundingBox().size.width + 10; 
		}
		spawn_timer = 0; 
	}

	if (shooting_timer == 0)
	{
		auto projectile = Sprite::create("rocket.png");
		auto move = MoveBy::create(5, Vec2(0, 980));
		this->addChild(projectile, -1);
		projectile->setPosition(player->getPosition().x, player->getPosition().y);
		projectile->runAction(move);
		projectiles.push_back(projectile);
		shooting_timer = 30;
	}
	enemy_timer--;
	
	if (shooting) shooting_timer--;
}